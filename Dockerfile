FROM rust:1.40.0-slim-stretch AS compile-image

RUN apt-get update && apt-get install -y --no-install-recommends \
    libglib2.0-dev \
    python-dev \
    python-libxml2 \
    && rm -rf /var/lib/apt/lists/*

ARG VERSION
RUN cargo install deno --version ${VERSION}


FROM debian:buster-20191224-slim

ARG GID=20000
ARG UID=20000
ARG GECOS=",,,,"
RUN \
  addgroup \
    --gid ${GID} \
    appgroup \
  && \
  adduser \
    --uid ${UID} \
    --gecos ${GECOS} \
    --gid ${GID} \
    --disabled-password \
    --disabled-login \
    appuser
USER ${UID}:${GID}

COPY --chown=root:staff --from=compile-image /usr/local/cargo/bin/deno /usr/local/bin/deno
